#!/usr/bin/env python
import rospy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist, Point, PointStamped
import math
import numpy as np
import tf
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import sys
import tf2_ros
import tf2_geometry_msgs
import threading
from std_srvs.srv import Empty

ball_dis, ball_ang, ball_dir = float('inf'), 0.0, np.array([1.0, 0.0, 0.0])
enemy_info = [(float('inf'), 0, np.array([0, 0, 265.23]))] * 4
enemy_history = [[[float('inf')] * 2] * 30] * 4
start_x, start_y = -100.0, -100.0
spd_pre, angspd_pre = 0.25, 0.0 #avoid sliping
score_pre = 0
spell_pre = 0
field = np.zeros((20, 20))
ball_x_history = []
ball_y_history = []
lock = threading.Lock()
stand_by = True
history_cool = 0
suggest=[0,0]
spell_cool = 0
spelled = False
spell_ball = False

def updateBallPos(msg):
  global bridge, ball_dis, ball_ang, ball_dir, enemy_info
  global lock
  global suggest
  cv_image = bridge.imgmsg_to_cv2(msg, "bgr8")
  hsv_image = cv2.cvtColor(cv_image,cv2.COLOR_BGR2HSV)
  msk_image = cv2.inRange(hsv_image, (155, 16, 0), (179, 255, 255))+cv2.inRange(hsv_image, (0, 16, 0), (25, 255, 255))
  ######
  hsv_copy = cv_image.copy()
  if suggest[0]==-1:
    cv2.circle(hsv_copy, (30, 50), 10, (100, 100, 255), -1)
  if suggest[1]==-1:
    cv2.circle(hsv_copy, (50, 30), 10, (255, 100, 100), -1)
  if suggest[1]==1:
    cv2.circle(hsv_copy, (10, 30), 10, (255, 100, 100), -1)
  cv2.imshow("Processed image", hsv_copy)
  cv2.waitKey(1)
  ######
  dis, ang, dir = determinePos(hsv_image, msk_image, 0.15, True)
  enemy_list = list(map(lambda r: determinePos(hsv_image, cv2.inRange(hsv_image, (r[0], 16, 0), (r[1], 255, 255)), 0.075, False), [[115, 124], [145, 154], [85, 94], [55, 64]]))
  lock.acquire()
  ball_dis, ball_ang, ball_dir = dis, ang, dir
  enemy_info = enemy_list
  lock.release()

def commandMove(msg):
  global stand_by, start_x, start_y
  global pub_cmd, ball_dis, ball_ang, ball_dir, enemy_info, enemy_history
  global spd_pre, angspd_pre, score_pre, spell_pre, field
  global ball_x_history, ball_y_history, history_cool
  global lock
  global suggest
  global spell_cool
  global spelled, spell_ball
  use_spellk = False
  use_spellp = False
  #basis
  x = msg.pose.pose.position.x
  y = msg.pose.pose.position.y
  ori = msg.pose.pose.orientation
  ang = tf.transformations.euler_from_quaternion([ori.x, ori.y, ori.z, ori.w])[2]

  #whether keep stand_by
  if stand_by:
    if start_x==-100.0:
      start_x, start_y = x, y
      return
    elif abs(x-start_x)<0.1 and abs(y-start_y)<0.1:
      cmd = Twist()
      cmd.linear.x=0.25
      pub_cmd.publish(cmd)
      return
    else:
      stand_by=False

  #check score
  if history_cool>0:
    history_cool-=1
  score_now, spell_now = rospy.get_param('score'), rospy.get_param('spell') 
  if score_now>score_pre or spell_now>spell_pre :
    rospy.loginfo("Ball's position change!")
    history_cool=15
    spell_cool=15
    if score_now>score_pre:
      score_pre=score_now
    resetField()
  elif spelled>0:
    spelled-=1
    if spell_now < spell_pre:
      spell_cool = 150
      spelled = 0
      if spell_ball:
        resetField()

  #avoid enemy
  lock.acquire()
  suggest = [0, 0]#suggestion for linear and angular to avoid enemy
  enemy_dis = list(map(lambda t: t[0], enemy_info))
  enemy_ang = list(map(lambda t: t[1], enemy_info))
  enemy_pos = list(map(lambda t: [float('inf')] * 2 if np.isinf(t[0]) else transToOdom(x, y, ang, t[0], t[2]).tolist(), enemy_info))
  for n in range(4):
    enemy_history[n] = enemy_history[n][1:] + [enemy_pos[n]]
  nearest = min(enemy_dis)
  if not np.isinf(nearest):
    robo_near = enemy_dis.index(nearest)
    if nearest<1.25:
      near_pos = np.array(enemy_pos[robo_near])
      now_ang = normAngle(math.atan2(near_pos[1]-y, near_pos[0]-x)-ang)
      usable_ind = list(map(lambda p: np.isinf(p[0]), enemy_history[robo_near])).index(False)
      temp = 29.1 / (29.1 - usable_ind) / 2
      future = near_pos + ((near_pos - np.array(enemy_history[robo_near][usable_ind])) * temp )
      angdif_f = normAngle(math.atan2(future[1]-y, future[0]-x)-ang)-now_ang
      appr = np.linalg.norm(near_pos - np.array([x, y])) - np.linalg.norm(future - np.array([x, y]))
      if nearest<0.3 or (nearest<0.4 and abs(nearest * math.cos(now_ang)) < 0.1 and appr>-spd_pre/2):
        suggest[0] = -1
      if appr>-spd_pre/2+0.05:
        if now_ang>0:
          if angdif_f < (45-now_ang)*0.2:
            if angdif_f > -now_ang*0.5:
              suggest[1] = -1
            else:
              suggest[1] = 1
        else:
          if angdif_f > -(now_ang+45)*0.2:
            if angdif_f < -now_ang*0.5:
              suggest[1] = 1
            else:
              suggest[1] = -1

  #decide cmd and spell
  cmd = Twist()
  if spell_cool > 0:
    spell_cool -= 1
  if np.isinf(ball_dis):
    enemy_exist = [False, False]
    for enemy in enemy_pos:
      if np.isinf(enemy[0]): continue
      selfenemy_ang = normAngle(math.atan2(enemy[1] - y, enemy[0] - x) - ang)
      enemyself_dis = math.sqrt(math.pow((enemy[0] - x),2)) + math.pow((enemy[1] - y),2)
      enemy_hit = enemyself_dis < 2.9 and abs(selfenemy_ang) < math.pi / 6 * 0.95
      if enemy_hit:
        if enemyself_dis < 0.5:
          if selfenemy_ang >= 0: enemy_exist[0] = True
          else: enemy_exist[1] = True
        if enemy_exist[0] and enemy_exist[1] or enemyself_dis <= 0.25 and spell_now > 1:
          try:
            rospy.ServiceProxy("/kamehameha", Empty)()
          except rospy.ServiceException as e:
            pass
          spell_cool=6
          break
    if len(ball_x_history)>2:
      angdif = normAngle(math.atan2(np.median(ball_y_history)-y, np.median(ball_x_history)-x) - ang)
      if abs(angdif)<math.pi/12:
        ball_x_history = ball_x_history[3:]
        ball_y_history = ball_y_history[3:]
      elif abs(angdif)<math.pi/6:
        ball_x_history = ball_x_history[1:]
        ball_y_history = ball_y_history[1:]
      cmd.linear.x = (1-abs(angdif/math.pi)*2)*0.26
      cmd.angular.z = angdif+np.sign(angdif)*0.01
    else:
      #rospy.loginfo("updating field")
      if np.where(field==0)[0].size==0:
        resetField()
      (row, col) = np.where(field==0)
      exp = 0.0
      dist_exp = 0.0
      for (gy, gx) in zip(row, col):
        ty, tx = (gy-9.5)*0.25-y, (gx-9.5)*0.25-x
        angtrg = math.atan2(ty, tx)
        angdif_ac = (angtrg - ang) % (math.pi *2)
        angdif_c = math.pi*2 - angdif_ac
        if angdif_ac < math.pi / 6 or angdif_c < math.pi / 6:
          field[gy, gx] = 1
        else:
          exp += angdif_ac - angdif_c
          for_pos = [x + math.cos(ang) * 0.05, y + math.sin(ang) * 0.05]
          back_pos = [x - math.cos(ang) * 0.05, y - math.sin(ang) * 0.05]
          dist_exp += np.linalg.norm(np.array([tx - for_pos[0], ty - for_pos[1]])) - np.linalg.norm(np.array([tx - back_pos[0], ty - back_pos[1]]))
      exp /= row.size * 0.5
      dist_exp /= row.size * 0.25
      #cmd.linear.x = np.sign(-dist_exp)*0.25
      cmd.angular.z = np.sign(angspd_pre/0.1/15-exp)*0.5
  else:
    #rospy.loginfo("adding ball history")
    odom_point = transToOdom(x, y, ang, ball_dis, ball_dir)
    if history_cool==0:
      ball_x_history += [odom_point[0]]
      ball_y_history += [odom_point[1]]
      if len(ball_x_history)>100:
        ball_x_history = ball_x_history[1:]
        ball_y_history = ball_y_history[1:]
    #rospy.loginfo("%f, %f, %f, %f", odom_point[0], odom_point[1], ball_dis, spd_pre)
    cmd.linear.x = (1-abs(ball_ang/math.pi)*2)*0.26
    cmd.angular.z = -ball_ang+np.sign(-ball_ang)*0.01
    #whether use spell
    if spell_cool == 0 and spell_now > 0:
      selfball_dis = np.linalg.norm(odom_point - np.array([x, y]))
      selfball_ang = normAngle(math.atan2(odom_point[1] - y, odom_point[0] - x) - ang)
      ball_hit = ball_dis < 2.9 and abs(selfball_ang) < math.pi / 6 * 0.95
      enemy_exist = [False, False]
      ang_match = False
      for enemy in enemy_pos:
        if np.isinf(enemy[0]): continue
        enemyball_dis = math.sqrt(math.pow((enemy[0] - odom_point[0]),2)) + math.pow((enemy[1] - odom_point[1]),2)
        enemyself_dis = math.sqrt(math.pow((enemy[0] - x),2)) + math.pow((enemy[1] - y),2)
        selfenemy_ang = normAngle(math.atan2(enemy[1] - y, enemy[0] - x) - ang)
        enemy_hit = enemyself_dis < 2.9 and abs(selfenemy_ang) < math.pi / 6 * 0.95
        if enemy_hit:
          if enemyself_dis < 0.5:
            if selfenemy_ang >= 0: enemy_exist[0] = True
            else: enemy_exist[1] = True
          if abs(selfenemy_ang-selfball_ang)<math.pi/12:
            ang_match = True
          if ball_hit:
            if enemyball_dis < selfball_dis:
              if abs(normAngle(selfenemy_ang - math.atan2(-y, -x))) < math.pi / 3:
                use_spellp = True
                rospy.loginfo("ebp")
              else:
                use_spellk = True
                rospy.loginfo("ebk")
          else:
            if enemyball_dis < selfball_dis and selfball_dis - enemyball_dis < 0.75 and abs(selfenemy_ang)>math.pi/9:
              use_spellp = True
              rospy.loginfo("ep")
            if enemy_exist[0] and enemy_exist[1] or enemyself_dis <= 0.25 and spell_now > 1:
              use_spellk = True
              rospy.loginfo("ek")
        elif ball_hit:
          if enemyball_dis < selfball_dis and spell_now > 2:
            use_spellp = True
            rospy.loginfo("bp")
      if ang_match and not ball_hit:
        use_spellk = False
    if (use_spellk or use_spellp) and spelled==0:
      spelled = 6

  #introduce suggestion
  if suggest[0] == -1 and ball_dis > 1:
    cmd.linear.x=-0.25
  if abs(cmd.linear.x)>0.05:
    if suggest[1] == -1:
      cmd.angular.z=-0.5
    elif suggest[1] == 1:
      cmd.angular.z=0.5
  #end suggestion

  lock.release()
  cmd.linear.x = cmd.linear.x if abs(cmd.linear.x - spd_pre) < 0.01 else np.sign(cmd.linear.x - spd_pre)*0.01+spd_pre
  cmd.angular.z = cmd.angular.z if abs(cmd.angular.z - angspd_pre) < 0.05 else np.sign(cmd.angular.z - angspd_pre)*0.05+angspd_pre
  spd_pre = cmd.linear.x
  angspd_pre = cmd.angular.z
  pub_cmd.publish(cmd)
  #rospy.loginfo("%f, %f", cmd.linear.x, cmd.angular.z)
  spell_pre=spell_now-(1 if use_spellk or use_spellp else 0)
  try:
    if use_spellk:
      rospy.ServiceProxy("/kamehameha", Empty)()
      spell_cool=6
      spell_ball = np.isinf(ball_dis)
    elif use_spellp:
      rospy.ServiceProxy("/petrificus", Empty)()
      spell_cool=6
      spell_ball = np.isinf(ball_dis)
  except rospy.ServiceException as e:
    pass





#returns (distance, angle, direction vector) toward target
def determinePos(hsv_image, msk_image, rad, sphere):
  red_count = np.count_nonzero(msk_image, axis=1)
  rows = np.where(red_count == max(red_count))[0]
  reds = np.nonzero(msk_image[rows[rows.size/2]])[0]
  if reds.size==0:
    return (float('inf'), 0, np.array([0, 0, 265.23]))
  else:
    left = min(reds)+0.5-hsv_image[rows[rows.size/2], min(reds), 1]/255.0
    right = max(reds)-0.5+hsv_image[rows[rows.size/2], max(reds), 1]/255.0
    if left<0 and right>319:
      return (0.15, 0, np.array([0, 0, 265.23]))
    else:
      if left<0 or right>319:
        if sphere:
          u=reds.size
          if left<0:
            u-=1-hsv_image[rows[rows.size/2], max(reds), 1]/255.0
            right-=1-hsv_image[rows[rows.size/2], max(reds), 1]/255.0
          else:
            u-=1-hsv_image[rows[rows.size/2], min(reds), 1]/255.0
            left+=1-hsv_image[rows[rows.size/2], min(reds), 1]/255.0
          v=np.count_nonzero(red_count)/2.0
          dia=(u**2+v**2)/u
          if left<0:
            left=right-dia
          else:
            right=left+dia
        else:
          v = np.count_nonzero(red_count)
          if left<0:
            left = right - v
          else:
            right = left + v
      elif right-left==1:
        left+=(1-hsv_image[rows[rows.size/2], min(reds), 1]/255.0)/2
        right-=(1-hsv_image[rows[rows.size/2], max(reds), 1]/255.0)/2
      left_ang, right_ang = math.atan((left-159.5)/265.23), math.atan((right-159.5)/265.23)
      ball_ang = (left_ang + right_ang)/2
      l_vec = np.array([left-159.5, rows[rows.size/2]-119.5, 265.23])
      r_vec = np.array([right-159.5, rows[rows.size/2]-119.5, 265.23])
      theta = np.arccos(np.inner(l_vec, r_vec)/np.linalg.norm(l_vec)/np.linalg.norm(r_vec))
      ball_dir = np.array([np.tan(ball_ang), rows[rows.size/2]-119.5 , 265.23])
      return (rad/math.sin(theta/2), ball_ang, ball_dir)

def transToOdom(x, y, ang, trg_dis, trg_dir):
  foot = np.dot(np.array([[0.0008, -0.1723, 0.985], [-1.0, 0.0, 0.0008]]), np.array([trg_dir/np.linalg.norm(trg_dir)*trg_dis]).transpose()) + np.array([[0.0445], [0.0]])
  foot[1, 0]*=100
  return np.dot(np.array([[np.cos(ang), -np.sin(ang)], [np.sin(ang), np.cos(ang)]]), foot).transpose()[0] + np.array([x, y])
    
def resetField():
  global field, ball_x_history, ball_y_history
  field=np.zeros((20,20))
  ball_x_history=[]
  ball_y_history=[]
  rospy.loginfo("Search field is cleared!")

def normAngle(ang):
  if ang > math.pi:
    return ang - math.pi*2
  elif ang <= -math.pi:
    return ang + math.pi*2
  else:
    return ang

def main():
  global pub_cmd, bridge, tf_buffer
  rospy.init_node('agent')
  bridge = CvBridge()
  rospy.Subscriber("odom", Odometry, commandMove)
  rospy.Subscriber("camera/image", Image, updateBallPos)
  pub_cmd = rospy.Publisher('cmd_vel', Twist, queue_size=10)
  try:
    rospy.spin()
  except KeyboardInterrupt:
    print("Shutting down")

if __name__ == '__main__':
  main()
