#!/usr/bin/env python
import rospy
import numpy as np
from nav_msgs.msg import Odometry
from nav_msgs.srv import GetPlan
from geometry_msgs.msg import PoseStamped, PolygonStamped
from std_srvs.srv import Empty
import copy

robot = np.array([-0.5, 0.0])
robot_pose = None
ghost = []
goal = [10000, 10000]

def ghost_callback(mes):
    global ghost
    ghost = list(map(lambda p: np.array([p.x, p.y]), mes.polygon.points))

def odom_callback(mes):
    global robot, robot_pose
    robot = np.array([mes.pose.pose.position.x, mes.pose.pose.position.y])
    robot_pose = PoseStamped(header=mes.header, pose=mes.pose.pose)
    if dist_from_robot(goal)<0.4:
        rospy.wait_for_service('/buster')
        rospy.ServiceProxy('/buster', Empty)()
            

def dist_from_robot(pos):
    global robot
    return np.linalg.norm(pos-robot)

if __name__ == '__main__':
    rospy.init_node('ghost_buster')
    rospy.Subscriber('/odom', Odometry, odom_callback)
    rospy.Subscriber('/ghost', PolygonStamped, ghost_callback)
    pub = rospy.Publisher('move_base_simple/goal', PoseStamped, queue_size=10)
    r = rospy.Rate(1)
    while not rospy.is_shutdown():
        ghost.sort(key=dist_from_robot)
        dist_near = 10000
        ghost_near = robot
        ghost_pose = None
        for g in ghost:
            rospy.loginfo(dist_near)
            if dist_from_robot(g) >= dist_near:
                break
            ghost_pose = copy.deepcopy(robot_pose)
            ghost_pose.pose.position.x = g[0]
            ghost_pose.pose.position.y = g[1]
            #try:   
            rospy.wait_for_service('/move_base/make_plan')
            path = rospy.ServiceProxy('/move_base/make_plan', GetPlan)(robot_pose, ghost_pose, 0.3).plan
            points = list(map(lambda p: np.array([p.pose.position.x, p.pose.position.y]), path.poses))
            leng = 0
            for n in range(0, len(points)-1):
                leng += np.linalg.norm(points[n+1] - points[n])
            rospy.loginfo(leng)
            if leng < dist_near:
                dist_near = leng
                ghost_near = g
            #except rospy.ServiceException as e:
            #    rospy.loginfo(e)
            #    pass
        goal = ghost_near
        if ghost_pose != None:
            pub.publish(ghost_pose)
        r.sleep()