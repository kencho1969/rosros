#!/usr/bin/env python
import rospy
import tf
import tf2_ros
import tf2_geometry_msgs
import math
import threading
import geometry_msgs
from geometry_msgs.msg import PointStamped, Point, Vector3
from visualization_msgs.msg import Marker
import sensor_msgs
from nav_msgs.msg import Odometry
from std_srvs.srv import Empty, EmptyResponse
import tf
import numpy as np

x, y = 0, 0
goal_x, goal_y = 0, 0
ori = None
tf_buffer = None
goto_flag = False
def goal_callback(message):
  global goal_x, goal_y, tf_listener
  ranges = map(lambda r: message.range_max if r==0 or r==float("inf") else r, message.ranges) 
  dist = min(ranges)
  if dist == message.range_max :
    rospy.loginfo("no object scanned")
    return
  angle = ranges.index(dist)*message.angle_increment+message.angle_min
  point = PointStamped(point=Point(dist*np.cos(angle), dist*np.sin(angle), 0))
  rospy.loginfo("Raw Goal := (%f, %f)", point.point.x, point.point.y)
  try:
    trans=tf_buffer.lookup_transform("odom", "base_scan", rospy.Time(0))
    odom_point = tf2_geometry_msgs.do_transform_point(point, trans)
    goal_x, goal_y = odom_point.point.x, odom_point.point.y
    rospy.loginfo("Goal := (%f, %f)", goal_x, goal_y)
  except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
    rospy.loginfo("goal_callback skipped")

def goal_listener():
  rospy.Subscriber("scan", sensor_msgs.msg.LaserScan, goal_callback)
  rospy.spin()

def odom_callback(message):
  global x, y, ori
  x = message.pose.pose.position.x
  y = message.pose.pose.position.y
  ori = message.pose.pose.orientation

def odom_listener():
  rospy.Subscriber("odom", Odometry, odom_callback)
  rospy.spin()

def cmd_publisher():
  global x, y, ori, goal_x, goal_y, goto_flag
  pub = rospy.Publisher('cmd_vel', geometry_msgs.msg.Twist, queue_size=10)
  pub_mrk = rospy.Publisher('marker_pub', Marker, queue_size=10)
  r = rospy.Rate(20)
  while not rospy.is_shutdown():
    marker = Marker()
    marker.header.frame_id='odom'
    marker.header.stamp=rospy.Time.now()
    marker.ns="closest"
    marker.id=0
    marker.action=Marker.ADD
    marker.pose.position=Point(goal_x, goal_y, 0.2)
    marker.color.g=1.0
    marker.color.b=1.0
    marker.color.a=1.0
    marker.scale=Vector3(0.05, 0.05, 0.4)
    marker.lifetime=rospy.Duration()
    marker.type=Marker.SPHERE
    pub_mrk.publish(marker)
    if not (ori is None) and goto_flag:
      dist = math.sqrt((goal_x-x)**2+(goal_y-y)**2)
      angtrg = math.atan2(goal_y-y, goal_x-x)
      angreal = tf.transformations.euler_from_quaternion([ori.x, ori.y, ori.z, ori.w])[2]
      angdif = (angtrg-angreal+math.pi)%(math.pi*2)-math.pi
      spd = (1-abs(angdif/math.pi)*2)*min(dist*2 if dist>0.3 else 0, 1)*0.25
      angspd = min(angdif*0.3+np.sign(angdif)*0.001, 0.5) if dist>0.3 else 0
      goto_flag = dist>0.3
      cmd = geometry_msgs.msg.Twist()
      cmd.linear.x=spd
      cmd.angular.z=angspd
      pub.publish(cmd)
    r.sleep()

def service_server():
  rospy.Service('goto_closest', Empty, handle_service)
  rospy.spin()

def handle_service(req):
  global goto_flag
  goto_flag=True
  return EmptyResponse()

if __name__ == '__main__':
  rospy.init_node('goal_manager')
  tf_buffer = tf2_ros.Buffer()
  tf_listener = tf2_ros.TransformListener(tf_buffer)
  threading.Thread(target=goal_listener).start()
  threading.Thread(target=odom_listener).start()
  threading.Thread(target=cmd_publisher).start()
  threading.Thread(target=service_server).start()


