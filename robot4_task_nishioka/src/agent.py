#!/usr/bin/env python
import rospy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist, Point, PointStamped
import math
import numpy as np
import tf
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import sys
import tf2_ros
import tf2_geometry_msgs
import threading

ball_dis, ball_ang, ball_dir = float('inf'), 0.0, np.array([1.0, 0.0, 0.0])
start_x, start_y = -100.0, -100.0
spd_pre, angspd_pre = 0.25, 0.0 #avoid sliping
score_pre = 0
spell_pre = 0
field = np.zeros((20, 20))
ball_x_history = []
ball_y_history = []
lock = threading.Lock()
stand_by = True
history_cool = 0

def updateBallPos(msg):
  global bridge, ball_dis, ball_ang, ball_dir
  global lock
  cv_image = bridge.imgmsg_to_cv2(msg, "bgr8")
  hsv_image = cv2.cvtColor(cv_image,cv2.COLOR_BGR2HSV)
  msk_image = cv2.inRange(hsv_image, (155, 16, 0), (179, 255, 255))+cv2.inRange(hsv_image, (0, 16, 0), (25, 255, 255))
  cv2.imshow("Processed image", msk_image)
  cv2.waitKey(1)
  red_count = np.count_nonzero(msk_image, axis=1)
  rows = np.where(red_count == max(red_count))[0]
  reds = np.nonzero(msk_image[rows[rows.size/2]])[0]
  lock.acquire()
  if reds.size==0:
    ball_dis = float('inf')
  else:
    left, right = min(reds)-0.5, max(reds)+0.5
    left = min(reds)+0.5-hsv_image[rows[rows.size/2], min(reds), 1]/255.0
    right = max(reds)-0.5+hsv_image[rows[rows.size/2], min(reds), 1]/255.0
    if left==-0.5 and right==319.5:
      ball_ang=0
      ball_dis=0.275
    else:
      if left==-0.5 or right==319.5:
        u=reds.size
        if left==-0.5:
          u-=1-hsv_image[rows[rows.size/2], max(reds), 1]/255.0
          right-=1-hsv_image[rows[rows.size/2], max(reds), 1]/255.0
        else:
          u-=1-hsv_image[rows[rows.size/2], min(reds), 1]/255.0
          left+=1-hsv_image[rows[rows.size/2], min(reds), 1]/255.0
        v=np.count_nonzero(red_count)/2.0
        dia=(u**2+v**2)/u
        if left==-0.5:
          left=right-dia
        else:
          right=left+dia
      elif right-left==1:
        left+=(1-hsv_image[rows[rows.size/2], min(reds), 1]/255.0)/2
        right-=(1-hsv_image[rows[rows.size/2], max(reds), 1]/255.0)/2
      else:
        left+=1-hsv_image[rows[rows.size/2], min(reds), 1]/255.0
        right-=1-hsv_image[rows[rows.size/2], max(reds), 1]/255.0
      left_ang, right_ang = math.atan((left-159.5)/265.23), math.atan((right-159.5)/265.23)
      ball_ang = (left_ang + right_ang)/2
      l_vec = np.array([left-159.5, rows[rows.size/2]-119.5, 265.23])
      r_vec = np.array([right-159.5, rows[rows.size/2]-119.5, 265.23])
      theta = np.arccos(np.inner(l_vec, r_vec)/np.linalg.norm(l_vec)/np.linalg.norm(r_vec))
      ball_dis = 0.15/math.sin(theta/2)
      ball_dir = np.array([np.tan(ball_ang), rows[rows.size/2]-119.5 , 265.23])
  lock.release()

def commandMove(msg):
  global stand_by, start_x, start_y
  global pub_cmd, ball_dis, ball_ang, ball_dir
  global spd_pre, angspd_pre, score_pre, spell_pre, field
  global ball_x_history, ball_y_history, history_cool
  global lock
  #basis
  x = msg.pose.pose.position.x
  y = msg.pose.pose.position.y
  ori = msg.pose.pose.orientation
  ang = tf.transformations.euler_from_quaternion([ori.x, ori.y, ori.z, ori.w])[2]

  #whether keep stand_by
  if stand_by:
    if start_x==-100.0:
      start_x, start_y = x, y
      return
    elif abs(x-start_x)<0.1 and abs(y-start_y)<0.1:
      cmd = Twist()
      cmd.linear.x=0.25
      pub_cmd.publish(cmd)
      return
    else:
      stand_by=False

  #check score
  if history_cool>0:
    history_cool-=1
  score_now, spell_now = rospy.get_param('score'), rospy.get_param('spell') 
  if score_now>score_pre or spell_now>spell_pre :
    rospy.loginfo("Ball's position change!")
    if score_now>score_pre:
      history_cool=15
      score_pre=score_now
    resetField()
    
  #decide cmd and spell
  lock.acquire()
  cmd = Twist()
  if np.isinf(ball_dis):
    if len(ball_x_history)>2:
      rospy.loginfo("using ball history %d", len(ball_x_history))
      angdif = math.atan2(np.median(ball_y_history)-y, np.median(ball_x_history)-x) - ang
      if angdif > math.pi:
        angdif -= math.pi*2
      elif angdif < -math.pi:
        angdif += math.pi*2
      if abs(angdif)<math.pi/12:
        ball_x_history = ball_x_history[3:]
        ball_y_history = ball_y_history[3:]
      elif abs(angdif)<math.pi/6:
        ball_x_history = ball_x_history[1:]
        ball_y_history = ball_y_history[1:]
      cmd.linear.x = (1-abs(angdif/math.pi)*2)*0.25
      cmd.angular.z = angdif+np.sign(angdif)*0.01
    else:
      rospy.loginfo("updating field")
      if np.where(field==0)[0].size==0:
        resetField()
      (row, col) = np.where(field==0)
      exp = 0.0
      dist_exp = 0.0
      for (gy, gx) in zip(row, col):
        ty, tx = (gy-9.5)*0.25-y, (gx-9.5)*0.25-x
        angtrg = math.atan2(ty, tx)
        angdif_ac = (angtrg - ang) % (math.pi *2)
        angdif_c = math.pi*2 - angdif_ac
        if angdif_ac < math.pi / 6 or angdif_c < math.pi / 6:
          field[gy, gx] = 1
        else:
          exp += angdif_ac - angdif_c
          for_pos = [x + math.cos(ang) * 0.05, y + math.sin(ang) * 0.05]
          back_pos = [x - math.cos(ang) * 0.05, y - math.sin(ang) * 0.05]
          dist_exp += np.linalg.norm(np.array([tx - for_pos[0], ty - for_pos[1]])) - np.linalg.norm(np.array([tx - back_pos[0], ty - back_pos[1]]))
      exp /= row.size * 0.5
      dist_exp /= row.size * 0.25
      #cmd.linear.x = np.sign(-dist_exp)*0.25
      cmd.angular.z = np.sign(angspd_pre/0.1/15-exp)*0.5
  else:
    rospy.loginfo("adding ball history")
    foot = np.dot(np.array([[0.0008, -0.1723, 0.985], [-1.0, 0.0, 0.0008]]), np.array([ball_dir/np.linalg.norm(ball_dir)*ball_dis]).transpose()) + np.array([[0.0445], [0.0]])
    odom_point = np.dot(np.array([[np.cos(ang), -np.sin(ang)], [np.sin(ang), np.cos(ang)]]), foot).transpose()[0] + np.array([x, y])
    if history_cool==0:
      ball_x_history += [odom_point[0]]
      ball_y_history += [odom_point[1]]
      if len(ball_x_history)>100:
        ball_x_history = ball_x_history[1:]
        ball_y_history = ball_y_history[1:]
    rospy.loginfo("%f, %f, %f, %f", odom_point[0], odom_point[1], ball_dis, spd_pre)
    cmd.linear.x = (1-abs(ball_ang/math.pi)*2)*0.25
    cmd.angular.z = -ball_ang+np.sign(-ball_ang)*0.01
  lock.release()
  cmd.linear.x = cmd.linear.x if abs(cmd.linear.x - spd_pre) < 0.01 else np.sign(cmd.linear.x - spd_pre)*0.01+spd_pre
  cmd.angular.z = cmd.angular.z if abs(cmd.angular.z - angspd_pre) < 0.1 else np.sign(cmd.angular.z - angspd_pre)*0.1+angspd_pre
  spd_pre = cmd.linear.x
  angspd_pre = cmd.angular.z
  pub_cmd.publish(cmd)
  #rospy.loginfo("%f, %f", cmd.linear.x, cmd.angular.z)
  spell_pre=spell_now #if use spell, minus 1 from right

def resetField():
  global field, ball_x_history, ball_y_history
  field=np.zeros((20,20))
  ball_x_history=[]
  ball_y_history=[]
  rospy.loginfo("Search field is cleared!")

def main():
  global pub_cmd, bridge, tf_buffer
  rospy.init_node('agent')
  bridge = CvBridge()
  rospy.Subscriber("odom", Odometry, commandMove)
  rospy.Subscriber("camera/image", Image, updateBallPos)
  pub_cmd = rospy.Publisher('cmd_vel', Twist, queue_size=10)
  try:
    rospy.spin()
  except KeyboardInterrupt:
    print("Shutting down")

if __name__ == '__main__':
  main()
