#!/usr/bin/env python
import rospy
from nav_msgs.msg import Odometry

def callback(message):
  rospy.loginfo("(x, y) = (%f, %f)", message.pose.pose.position.x, message.pose.pose.position.y)

def odom_listener():
  rospy.init_node('odom_listener')
  rospy.Subscriber("odom", Odometry, callback)
  rospy.spin()

if __name__ == '__main__':
    odom_listener()



