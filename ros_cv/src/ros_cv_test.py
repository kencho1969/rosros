#!/usr/bin/env python

import rospy
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError


def callbackImage(msg):
  global pub, bridge
    
  try:
    cv_image = bridge.imgmsg_to_cv2(msg, "bgr8")
    hsv_image = cv2.cvtColor(cv_image,cv2.COLOR_BGR2HSV)
  except CvBridgeError as e:
    rospy.logerr(e)

  hsv_image = cv2.inRange(hsv_image, (155, 64, 0), (179, 255, 255))+cv2.inRange(hsv_image, (0, 64, 0), (25, 255, 255))
  # plot image
  cv2.imshow("Processed image", hsv_image)
  cv2.waitKey(3)

  # convert to ROS format and publish
  try:
    pub.publish(bridge.cv2_to_imgmsg(hsv_image, "8UC1"))
  except CvBridgeError as e:
    rospy.logerr(e)

def main():

  global pub, bridge

  rospy.init_node('image_converter', anonymous=True)

  # subscribe to original image
  sub = rospy.Subscriber("/camera/image", Image, callbackImage)
  # publisher for processed image
  pub = rospy.Publisher("/processed/image", Image, queue_size=5);

  # CvBridge for converting Ros <-> OpenCV
  bridge = CvBridge()

  try:
    rospy.spin()
  except KeyboardInterrupt:
    print("Shutting down")
  cv2.destroyAllWindows()


if __name__ == '__main__':
    main()
