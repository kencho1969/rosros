#!/usr/bin/env python

import rospy

import cv2
import tf
import math
import numpy as np
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from geometry_msgs.msg import Point, PointStamped, Twist

import tf2_geometry_msgs 
import tf2_ros

pose_log = [0.0, 0.0]
ball_ang = 0.0

def imageCallback(imgMessage):
  global pub_detected_pose, cvbridge, tfBuffer, pose_log, ball_ang
  
  try:
    cv_image = cvbridge.imgmsg_to_cv2(imgMessage, "bgr8")
  except CvBridgeError as e:
    rospy.logerr(e)
  msk_image = cv2.inRange(cv_image, (0, 0, 0), (50, 50, 50))
  black_count = np.count_nonzero(msk_image, axis=1)
  rows = np.where(black_count == max(black_count))[0]
  blacks = np.nonzero(msk_image[rows[rows.size/2]])[0]
  if blacks.size==0:
    return
  left = min(blacks)-1+cv_image[rows[rows.size/2], min(blacks), 1]/56.0
  right = max(blacks)+1-cv_image[rows[rows.size/2], max(blacks), 1]/56.0
  left_ang, right_ang = math.atan((left-159.5)/265.23), math.atan((right-159.5)/265.23)
  ball_ang = (left_ang + right_ang)/2
  l_vec = np.array([left-159.5, rows[rows.size/2]-119.5, 265.23])
  r_vec = np.array([right-159.5, rows[rows.size/2]-119.5, 265.23])
  theta = np.arccos(np.inner(l_vec, r_vec)/np.linalg.norm(l_vec)/np.linalg.norm(r_vec))
  ball_dis = 0.15/math.sin(theta/2)
  ball_dir = np.array([np.tan(ball_ang)*265.23, rows[rows.size/2]-119.5 , 265.23])

  ball_pose = PointStamped()
  ball_pose.header.frame_id = imgMessage.header.frame_id
  ball_pose.header.stamp = rospy.Time()
  ball_pose.point = Point(*((ball_dir/np.linalg.norm(ball_dir)*ball_dis).tolist()))
  # converting ball pose to odom frame
  try:
    trans_to_odom = tfBuffer.lookup_transform("odom", ball_pose.header.frame_id, rospy.Time())
  except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
    rospy.logerr("Error in transforming to global frame")
    return
  ball_pose_odom = tf2_geometry_msgs.do_transform_point(ball_pose, trans_to_odom)
    
  # we do not care about the ball height (z) so we will reset it to 0
  ball_pose_odom.point.z = 0.0

  # ----------
  # Task 4-(4)
  # add temporal filtering to estimated pose
  ball_pose_odom.point.x = ball_pose_odom.point.x * 0.6 + pose_log[0] * 0.4
  ball_pose_odom.point.y = ball_pose_odom.point.y * 0.6 + pose_log[1] * 0.4
  pose_log = [ball_pose_odom.point.x, ball_pose_odom.point.y]
  # ----------
  
  pub_detected_pose.publish(ball_pose_odom)
  
  # plot image
  cv2.imshow("Original image", msk_image)
  cv2.waitKey(3)
  
	
def main():
  global pub_detected_pose, cvbridge, tfBuffer
  
  rospy.init_node('trackme_template')

  # tf2 buffer/listener
  tfBuffer = tf2_ros.Buffer()
  tfListener = tf2_ros.TransformListener(tfBuffer)
  
  # CvBridge for converting Ros <-> OpenCV
  cvbridge = CvBridge()

  # subscribe to robot image
  sub = rospy.Subscriber("camera/image", Image, imageCallback)
  
  # publisher for ball pose
  pub_detected_pose = rospy.Publisher('ball_pose', PointStamped, queue_size=10)
  
  # publisher for robot velocity command
  pub_movement = rospy.Publisher('cmd_vel', Twist, queue_size=10)
  

  rate = rospy.Rate(20.0)

  while not rospy.is_shutdown():
    
    movement = Twist()
    
    # ----------
    # Task 4-(3)
    # add code for robot control
    movement.angular.z = - ball_ang
    # ----------

    movement.linear.x = 0.0 # leave this value at 0!!
    pub_movement.publish(movement)

    rate.sleep()

  cv2.destroyAllWindows()

if __name__ == '__main__':
  main()


