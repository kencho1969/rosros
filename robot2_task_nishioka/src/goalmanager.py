#!/usr/bin/env python
import rospy
import tf
import math
import threading
import geometry_msgs
from nav_msgs.msg import Odometry
import tf
import numpy as np

x, y = 0, 0
goal_x, goal_y = 0, 2
ori = None
def goal_callback(message):
  global goal_x, goal_y
  goal_x, goal_y = message.pose.position.x, message.pose.position.y
  rospy.loginfo("Goal := (%f, %f)", goal_x, goal_y)

def goal_listener():
  rospy.Subscriber("move_base_simple/goal", geometry_msgs.msg.PoseStamped, goal_callback)
  rospy.spin()

def odom_callback(message):
  global x, y, ori
  x = message.pose.pose.position.x
  y = message.pose.pose.position.y
  ori = message.pose.pose.orientation
  rospy.loginfo("(x, y) = (%f, %f)", x, y)

def odom_listener():
  rospy.Subscriber("odom", Odometry, odom_callback)
  rospy.spin()

def cmd_publisher():
  global x, y, ori, goal_x, goal_y
  pub = rospy.Publisher('cmd_vel', geometry_msgs.msg.Twist, queue_size=10)
  r = rospy.Rate(20)
  while not rospy.is_shutdown():
    if not (ori is None):
      dist = math.sqrt((goal_x-x)**2+(goal_y-y)**2)
      angtrg = math.atan2(goal_y-y, goal_x-x)
      angreal = tf.transformations.euler_from_quaternion([ori.x, ori.y, ori.z, ori.w])[2]
      angdif = (angtrg-angreal+math.pi)%(math.pi*2)-math.pi
      rospy.loginfo("(trg, real, dif) = (%f, %f, %f)", angtrg, angreal, angdif)
      spd = (1-abs(angdif/math.pi)*2)*min(dist*2 if dist>0.05 else 0, 1)*0.25
      angspd = min(angdif*0.3+np.sign(angdif)*0.001, 0.5) if dist>0.05 else 0
      cmd = geometry_msgs.msg.Twist()
      cmd.linear.x=spd
      cmd.angular.z=angspd
      pub.publish(cmd)
    r.sleep()

if __name__ == '__main__':
  rospy.init_node('goal_manager')
  threading.Thread(target=goal_listener).start()
  threading.Thread(target=odom_listener).start()
  threading.Thread(target=cmd_publisher).start()


