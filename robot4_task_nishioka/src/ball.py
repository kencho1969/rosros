#!/usr/bin/env python

import rospy, math, cv2
import numpy as np
import threading
from tf.transformations import euler_from_quaternion
from nav_msgs.msg import Odometry
from geometry_msgs.msg import PoseStamped, Twist, Point, PointStamped, Quaternion
from std_srvs.srv import Trigger, TriggerResponse
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
#odom, camera/image, cmd_vel
class BallCatcher():
   def __init__(self):
       self.cmd_vel = rospy.Publisher('cmd_vel', Twist, queue_size=10)
       self.bridge = CvBridge()
       self.move_cmd = Twist()
       self.masked_image = np.empty((240,320))
              
       self.linear_speed = 0.25
       self.angular_speed = 0.5
       self.u_center = 160.5
       self.f = 265.23
       self.ball_radius = 0.15


       self.angle_center = 0.0
       self.angle_side_l = 0.0
       self.angle_side_r = 0.0
       self.distance2ball = 0.0
  
   def detect_red_color(self,img):
       hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV_FULL)
       h = hsv[:, :, 0]
       s = hsv[:, :, 1]
       mask = np.zeros(h.shape, dtype=np.uint8)
       mask[((h < 10)) & (s > 128)] = 255
       return mask


   def process(self,masked_image):
       mask = np.where(masked_image==255,1,0)
       col_sum = np.sum(mask, axis=1)
       col_num = np.argmax(col_sum)
       max_col = mask[col_num].tolist() #longest column


       u_left = max_col.index(1)
       u_right = len(max_col)-1-max_col[::-1].index(1)
       u_diff_l = u_left - self.u_center
       u_diff_r = u_right - self.u_center
       k_side_l = u_diff_l / self.f
       k_side_r = u_diff_r / self.f


       self.angle_side_l = math.atan(k_side_l)
       self.angle_side_r = math.atan(k_side_r)


       if u_diff_l > 0:
           turn_angle = -self.angle_side_l
       elif u_diff_r < 0:
           turn_angle = -self.angle_side_r
       else:
           turn_angle = 0.0
      
       if turn_angle != 0.0: self.move_cmd.angular.z = max(-self.angular_speed, min(0.95*turn_angle, self.angular_speed))
       rospy.loginfo("turn_angle: %s, ang_v: %s ", turn_angle, self.move_cmd.angular.z)


  
   def callback_Image(self,msg):   
       try:
           cv_image = self.bridge.imgmsg_to_cv2(msg, "bgr8")
       except CvBridgeError as e:
           rospy.logerr(e)


       self.masked_image = self.detect_red_color(cv_image)
       if self.masked_image.max() == 255:
           self.move_cmd.linear.x = self.linear_speed
           self.process(self.masked_image)
       else:
           rospy.loginfo("no ball:  ang_v: %s ", self.move_cmd.angular.z)
           self.move_cmd.linear.x = 0.0
           if self.move_cmd.angular <0: self.move_cmd.angular.z = -self.angular_speed
           else: self.move_cmd.angular.z = self.angular_speed
          
       cv2.imshow("Processed image", self.masked_image)
       cv2.waitKey(3)
  
   def image_listener(self):
       rospy.Subscriber("camera/image", Image, self.callback_Image)
       rospy.spin()


     
   def pub_vel(self):       
       r = rospy.Rate(20)
       while not rospy.is_shutdown():


           self.cmd_vel.publish(self.move_cmd)


           r.sleep()
  
  
if __name__ == '__main__':
  rospy.init_node('navigation')
  nav = BallCatcher()
  thread_img = threading.Thread(target=nav.image_listener)
  #thread_odom = threading.Thread(target=nav.odom_listener)
  thread_img.setDaemon(True)
  thread_img.setDaemon(True)


  thread_img.start()
  #thread_odom.start()
  nav.pub_vel()