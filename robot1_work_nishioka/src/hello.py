#!/usr/bin/env python
import rospy
from std_msgs.msg import String

def hello2():
  rospy.init_node('hello_ros')

  rate = rospy.Rate(10) 

  while not rospy.is_shutdown():

    hello_str = "Hello, ROS! %s" % rospy.get_time()
    rospy.loginfo(hello_str)

    rate.sleep()

hello2()



